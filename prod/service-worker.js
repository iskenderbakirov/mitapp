/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["/about.html","f279c7914ffb69263d8ab72eee9b7d32"],["/contacts.html","b37ebd96f2078a839b8fb230df7c5bf0"],["/css/bootstrap.min.css","e672c9a1cdd414aa32ad9ade6ddc8415"],["/css/style.min.css","0a39b2433a76731b3994fa8c30950411"],["/fonts/ProximaNova-Bold/ProximaNova-Bold.eot","10141a9d737da9c84e6e14ec5a562c6c"],["/fonts/ProximaNova-Bold/ProximaNova-Bold.svg","5f5ecd9a3a2b453937b385465922f765"],["/fonts/ProximaNova-Bold/ProximaNova-Bold.ttf","926a08fb27e3303c7452b0bdd2d5e5ab"],["/fonts/ProximaNova-Bold/ProximaNova-Bold.woff","2b6ad1b67abc19d3b55eb7d5ed89abd4"],["/fonts/ProximaNova-Bold/preview.html","620bb5f0e2522ab3a4dda0f6721c1792"],["/fonts/ProximaNova-Bold/styles.css","f02e53353f7446b820aef389a9fb9d20"],["/fonts/ProximaNova-Light/ProximaNova-Light.eot","d778218590f14376c366c515d384808e"],["/fonts/ProximaNova-Light/ProximaNova-Light.svg","c6940a610d1a08601b86b0a2437a2d2b"],["/fonts/ProximaNova-Light/ProximaNova-Light.ttf","0188899cd3ec92cdcaa4c97ca0c75247"],["/fonts/ProximaNova-Light/ProximaNova-Light.woff","bc18650d35f1be2c95ecec4dc0175c99"],["/fonts/ProximaNova-Light/preview.html","5ca1da80d0006cfd42b50b8a9d272888"],["/fonts/ProximaNova-Light/styles.css","272827e255fbaf121d3da7121928ce9c"],["/fonts/ProximaNova-Regular/ProximaNova-Regular.eot","b7c512788e3c77b0196f0bace8a88418"],["/fonts/ProximaNova-Regular/ProximaNova-Regular.svg","b7141825588b6cbdf9776b8b5754c6b0"],["/fonts/ProximaNova-Regular/ProximaNova-Regular.ttf","7ce6760d17685c466ba04d1b2c63c38b"],["/fonts/ProximaNova-Regular/ProximaNova-Regular.woff","61571ff74cc0fba6de49ace74846bfec"],["/fonts/ProximaNova-Regular/preview.html","3eefcd2e72d79bf9394f73a763b17814"],["/fonts/ProximaNova-Regular/styles.css","8d6e13c119c8d407f6e3ce2cd18dadc4"],["/fonts/ProximaNova-Semibold/ProximaNova-Semibold.eot","cddac6589ca25eb44179341774222e79"],["/fonts/ProximaNova-Semibold/ProximaNova-Semibold.svg","ac541d6e8e0dd349b72ad698bd2c143b"],["/fonts/ProximaNova-Semibold/ProximaNova-Semibold.ttf","df8c626474a73ab7a8b511655597c7c4"],["/fonts/ProximaNova-Semibold/ProximaNova-Semibold.woff","5082ba8e0c15946eb97e8394488c6d36"],["/fonts/ProximaNova-Semibold/preview.html","e8faa57cb9f85bcae3acb2f9a6369322"],["/fonts/ProximaNova-Semibold/styles.css","3d4d1da769595b4377bb8edfca7a4a77"],["/fonts/ProximaNovaT-Thin/ProximaNovaT-Thin.eot","c8516677b967f1e3212e6274da5be696"],["/fonts/ProximaNovaT-Thin/ProximaNovaT-Thin.svg","dfb132c717fa5555d9905fa051a1f076"],["/fonts/ProximaNovaT-Thin/ProximaNovaT-Thin.ttf","4a3961960c6827bf176eb0ef73653784"],["/fonts/ProximaNovaT-Thin/ProximaNovaT-Thin.woff","1adca8551b3f8ba1a18e41e5716ea74f"],["/fonts/ProximaNovaT-Thin/preview.html","7c786eb57330b5df8861e7d42c4a86f8"],["/fonts/ProximaNovaT-Thin/styles.css","ff60c3d6fa275178a15ca987ebc86463"],["/fonts/glyphicons-halflings-regular.eot","f4769f9bdb7466be65088239c12046d1"],["/fonts/glyphicons-halflings-regular.svg","89889688147bd7575d6327160d64e760"],["/fonts/glyphicons-halflings-regular.ttf","e18bbf611f2a2e43afc071aa2f4e1512"],["/fonts/glyphicons-halflings-regular.woff","fa2772327f55d8198301fdb8bcfc8158"],["/fonts/glyphicons-halflings-regular.woff2","448c34a56d699c29117adc64c43affeb"],["/img/banners/about.jpg","7c9c7918548fe282acb1ebbd807d481e"],["/img/banners/main.jpg","38955fafd470adfde20947468a6eb85e"],["/img/banners/news-inner.jpg","4914bf947a34111b838ef1e67587c2ad"],["/img/banners/news.jpg","36bd8b9a6ea68254b2061e781098d896"],["/img/banners/projects-inner.jpg","23ce08641d0a7b82248c0b143ab404f5"],["/img/banners/services-inner.jpg","2cf10d2ad749f1be61bde00a44fcd6c9"],["/img/banners/services.jpg","7c9c7918548fe282acb1ebbd807d481e"],["/img/icons/breadcrumbs-arrow.svg","77451e025a890f7a2b8f4d2d43dc344d"],["/img/icons/carousel-control.svg","2c0b7602694dfee804f7634d276eeb58"],["/img/icons/plus-main-color.svg","46aed10807c00f0052b22c03bd4e2ed5"],["/img/icons/presentation.svg","0bffceb6863e0a3a41dc4c543ddd26a5"],["/img/icons/project-title-symbol.svg","7799888db0b2f6f83d6dffef3054669c"],["/img/icons/project/1.svg","5acf39c07d02e2f0fc7f454b4ef98126"],["/img/icons/project/2.svg","a2cbf78353b1e7180f4b799352bf1279"],["/img/icons/project/3.svg","380bc50104c18ec2a6c59fec8b66b1e5"],["/img/icons/services/1.svg","61929c1dfd78d6ea63e4f68233e946d7"],["/img/icons/services/2.svg","2ecba74226153c75cbc8fa44ac61fb56"],["/img/icons/services/3.svg","4ff0b6bdf328beb942f29fa96d04f7f2"],["/img/icons/services/4.svg","380bc50104c18ec2a6c59fec8b66b1e5"],["/img/icons/services/5.svg","30e1c371eff9e0b640e5ee9654a8347c"],["/img/icons/services/6.svg","e1d20722737856ed26153e237e6984f0"],["/img/icons/services/plus.svg","610847c7dab74b90dc9c922407a5f40e"],["/img/icons/social/fc-hover.svg","d98cdbab41f585bab435bff4f530e517"],["/img/icons/social/fc.svg","8b630bba3b0da854648224359ca41e52"],["/img/icons/social/ins-hover.svg","26f6af5993ecde86f8b00a73bfe0c9f5"],["/img/icons/social/ins.svg","4428ea7383189e58a69bf5c241cca7d5"],["/img/jpg/carousel-item.png","fd811c34fb7b2b6ce88d9f26b6131fa7"],["/img/jpg/mockup.png","1da67665cccb934c3781e79e2dd1b3ac"],["/img/jpg/news-inner-content.jpg","dc4469f646313b7e0c42ddf8da8e274f"],["/img/jpg/news.jpg","7ed5bf75d9e8451b0bb1958d6ec2313c"],["/img/jpg/projects-item.jpg","af2b1bce2710bf82e6fbeb970ce078c4"],["/img/jpg/projects/1.jpg","af2b1bce2710bf82e6fbeb970ce078c4"],["/img/jpg/projects/2.jpg","0d473c5f7cc07c70d07d917fad2b3204"],["/img/jpg/projects/3.jpg","617709370611e98ebc637ea01b0bcbfc"],["/img/jpg/projects/4.jpg","0da08f60a36a563832c9c4e91b496c59"],["/img/jpg/review-user.jpg","02f1cf1d95a41b63391a173dde2fedaa"],["/img/jpg/team-item.jpg","e29e15417002183c78012bd331f38709"],["/img/logo.svg","1b6b7c9ae9a750cdc640edc094d700ca"],["/img/logo/clients/beeline.svg","49436d835f3f95a49e5ba81650b4fded"],["/img/logo/clients/integra.svg","15597cfb94efdff3f81857429aae7505"],["/img/logo/clients/obse.svg","68078ae81a9482f0dfc4c4934e483db6"],["/img/logo/clients/veon.svg","8e0aafc4f91975a4b0255b0dc37893f5"],["/img/logo/partners/partner_beeline.svg","49436d835f3f95a49e5ba81650b4fded"],["/img/logo/partners/partner_integra.svg","15597cfb94efdff3f81857429aae7505"],["/img/logo/partners/partner_veon.svg","8e0aafc4f91975a4b0255b0dc37893f5"],["/img/logo/tech/android.svg","ecf10822e012b2972a06a97f537fa770"],["/img/logo/tech/asp.svg","83265eb36cbc15efd161ca396c673013"],["/img/logo/tech/c.svg","992bd311e540fa2782d4c8168c6f37d2"],["/img/logo/tech/java.svg","60ccc205dacddb032d25fa2ede0d0d01"],["/img/logo/tech/js.svg","a72689ce489e06b395884239864baca1"],["/img/logo/tech/swift.svg","46162d208a6393c60841bb92755b7c49"],["/img/shadow.svg","541c2117fc4edb291127afa2a5972247"],["/index.html","4325c7114ce758d8a99460cb05345390"],["/js/bootstrap.min.js","f78e57eedc67e2e1ae34815833e7eec3"],["/js/common.min.js","7ca61c3c74570affc803b49fa5c96e23"],["/js/jquery-ui-1.9.2.min.js","7368211102cd69dfb5930379c7948a0e"],["/js/libs.min.js","f70dd4394e45837cbae608cc6a3cacb7"],["/js/mixitup.min.js","20c383399604b2b09cfc9c434f2465af"],["/manifest.json","868a2a1a77f23a12c3b779bb26834da2"],["/news-inner.html","2376f79b3f9918b58587a348ba8d6a63"],["/news.html","40ac10d4efc079806d96310582ee9c3b"],["/projects-inner.html","d4adfa398d22971fbbb1c12d27921002"],["/projects.html","5b964ae8438682dc4e6f19f617711a9d"],["/services-inner.html","2aeb396ee5c417a464b93fe0b938c2a0"],["/services.html","f32b4be5d53259813316fed48c5659a5"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function(originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function(originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function(originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function(whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function(originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







