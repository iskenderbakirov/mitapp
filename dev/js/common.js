
$(document).ready(function () {

	function setHeight (item, width, height) {
		var itemWidth = item.width();
		item.css({
			height: (itemWidth / width) * height
		});
	}

	setHeight($('.services-list li'), 38, 39);
	setHeight($('.projects-page-page-list li'), 12, 13);
	setHeight($('.news-img-wrapper'), 3, 2);
	setHeight($('.team-list li'), 59, 64);
	setHeight($('.services-inner-projects li, .project-carousel-item'), 19, 26);

	$('#projects-list').mixItUp();

	if ($(window).width() > 600) {
		// review carousel
		$('.multi-item-carousel').carousel({
			interval: false
		});

		$('.multi-item-carousel .item').each(function(){
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));
		});
	}


});